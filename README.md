# README #

Thank you for visiting the website of meisel. 

The goal of meisel is to boost the process of learning vocabulary in other languages. 
I'm writing it to learn Java and connected technologies as well as libraries useful in further work.

## What is this repository for? ##

This repository contains the source code as well as an example quiz file, called "words.xml". 

It's still a development version (most likely will also remain in this status). Therefore i do decline all reposponsibility in terms of absolutely anything. You download, compile and run it for your own risk. On the other hand i hope it might be useful to someone.

## Build, run ##

### Build ###

The best way to compile it, is to use Maven (https://maven.apache.org/). The latest one should run, i'm using it for builds. Follow the installation instructions on the provided website. After successfull installation, perform:

git clone https://serserser@bitbucket.org/serserser/meisel.git
cd meisel
mvn install

After that folder named 'target' should appear. There you can find meisel.jar and folder named 'lib' (these parts are important). You can copy them anywhere you like.

### Run ###

You run it by executing command:

java -jar meisel.jar


## Contribution guidelines ##

I would be grateful, if you could send me any feedback (absolutely any, even "I've installed your program. It sucks all the way down" also does the job :) ). Even if you don't even download it, but you like the idea - let me know. 
Please send me ideas, where can I go next with this application. 

If you have any enhancements for meisel, please send them back to me. It's not obligatory, but it would be very nice.

The best way to contact me is to send an email to:
jacek_golda@wp.pl

## Plans for future - TODO list ##

1. Finish basic functionality:
1.1 Quiz editor
1.2 "Performing quizes" - the main part for learning
1.3 User preferences - saving and using them in a reasonable way
2. Possibly storing tests in database?
3. Some "extra" like changing skins

Once more, thank you for visiting this website, 
Jacek Gołda