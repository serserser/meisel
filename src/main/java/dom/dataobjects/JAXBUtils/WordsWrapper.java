package dom.dataobjects.JAXBUtils;

import dom.dataobjects.Word;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
public class WordsWrapper
{
	@XmlElement(name="word")
	private ObservableList<Word> words;

	public WordsWrapper()
	{
		words = FXCollections.observableArrayList();
	}

	public ObservableList<Word> getWords()
	{
		return words;
	}

	public void setWords(ObservableList<Word> words)
	{
		this.words = words;
	}
}
