package dom.dataobjects;

import dom.Utils.BaseClass;
import dom.dataobjects.JAXBUtils.WordsWrapper;
import javafx.collections.ObservableList;

import javax.xml.bind.annotation.*;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
public class Quiz extends BaseClass
{
	public enum Status
	{
		SAVED,
		UNSAVED
	}

	public enum SavingStatus
	{
		SUCCESS,
		USER_CANCELED,
		ERROR_OCCURRED
	}

	public enum ClosingStatus
	{
		SAVE_REQUESTED,
		CANCEL_OPERATION,
		CAN_BE_CLOSED
	}

	@XmlElement
	private String name;
	@XmlElement
	private String description;
	@XmlElement(name="firstColumn")
	private String firstColumnLanguage;
	@XmlElement(name="secondColumn")
	private String secondColumnLanguage;
	@XmlElement(name="words")
	private WordsWrapper wordsWrapper;
	@XmlTransient
	private Status status;

	public Quiz()
	{
		name = "";
		description = "";
		firstColumnLanguage = "";
		secondColumnLanguage = "";
		wordsWrapper = new WordsWrapper();
		status = Status.SAVED;
	}

	public String getName()
	{
		return name;
	}


	public void setName(String name)
	{
		this.name = name;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public String getFirstColumnLanguage()
	{
		return firstColumnLanguage;
	}

	public void setFirstColumnLanguage(String firstColumnLanguage)
	{
		this.firstColumnLanguage = firstColumnLanguage;
	}

	public String getSecondColumnLanguage()
	{
		return secondColumnLanguage;
	}

	public void setSecondColumnLanguage(String secondColumnLanguage)
	{
		this.secondColumnLanguage = secondColumnLanguage;
	}

	private ObservableList<Word> words()
	{
		return wordsWrapper.getWords();
	}

	public Status getStatus()
	{
		return status;
	}

	public void setStatus(Status status)
	{
		this.status = status;
	}

	public ObservableList<Word> getWords()
	{
		return words();
	}

	public void addWord(Word word)
	{
		words().add(word);
	}

	public void removeWords(List<Word> words)
	{
		words().removeAll(words);
	}
}
