package dom.Utils;


import javafx.stage.FileChooser;
import java.io.File;

public class FileOperations
{
	public static String getOpenFilePath(String title)
	{
		if ( title == null ) title = "Choose file to open";

		FileChooser chooser = new FileChooser();
		chooser.setTitle(title);
		File result = chooser.showOpenDialog(null);
		if ( result == null )
			return null;
		String filePath = result.getAbsolutePath();
		return filePath;
	}

	public static String getSaveFilePath(String title)
	{
		if ( title == null ) title = "Choose file to save";

		FileChooser chooser = new FileChooser();
		chooser.setTitle(title);
		File result = chooser.showSaveDialog(null);
		if ( result == null )
			return null;
		String filePath = result.getAbsolutePath();
		return filePath;
	}
}
