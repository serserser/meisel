package dom.Utils.Exceptions;

public class NoMoreWordsException extends IllegalStateException
{
	public NoMoreWordsException(String message)
	{
		super(message);
	}
}
