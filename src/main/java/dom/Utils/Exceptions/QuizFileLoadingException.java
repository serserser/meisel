package dom.Utils.Exceptions;

public class QuizFileLoadingException extends RuntimeException
{
	public QuizFileLoadingException(String message)
	{
		super(message);
	}
}
