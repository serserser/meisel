package dom.Utils;

import org.apache.log4j.Logger;

public abstract class BaseClass
{
	protected static Logger logger;

	public BaseClass()
	{
		logger = Logger.getLogger(getClass());
	}

	protected static void logStart(String methodName)
	{
		logger.debug(methodName + " start");
	}

	protected static void logEnd(String methodName)
	{
		logger.debug(methodName + " finished");
	}
}
