package dom.Utils;

import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;

public class UnsavedChangesAlert extends Alert
{
	public UnsavedChangesAlert()
	{
		super(AlertType.WARNING);
		setButtons();
	}

	public UnsavedChangesAlert(String contextText, String headerText, String title)
	{
		super(AlertType.WARNING);
		setContentText(contextText);
		setHeaderText(headerText);
		setTitle(title);

		setButtons();
	}

	private void setButtons()
	{
		getButtonTypes().clear();
		getButtonTypes().addAll(ButtonType.YES, ButtonType.NO, ButtonType.CANCEL);

		Button yesButton = (Button) getDialogPane().lookupButton(ButtonType.YES);
		Button noButton = (Button) getDialogPane().lookupButton(ButtonType.NO);
		Button cancelButton = (Button) getDialogPane().lookupButton(ButtonType.CANCEL);

		yesButton.setDefaultButton(true);
		noButton.setDefaultButton(false);
		cancelButton.setDefaultButton(false);
	}
}
