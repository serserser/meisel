package dom.Utils;


import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import java.util.Optional;

public class InformationDialog
{
	public static void show(String content, String header, String title)
	{
		Alert alert = new Alert(Alert.AlertType.INFORMATION);
		alert.setTitle(title);
		alert.setHeaderText(header);
		alert.setContentText(content);

		alert.showAndWait();
	}

	public static void show(String content, String header)
	{
		show(content, header, "");
	}

	public static void show(String content)
	{
		show(content, "", "");
	}
}
