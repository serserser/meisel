package dom.meisel.dataobjects;

import dom.meisel.Utils.BaseClass;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
public class Word extends BaseClass
{
	@XmlElement
	private String firstOption;
	@XmlElement
	private String secondOption;

	public Word(String firstOption, String secondOption)
	{
		this.firstOption = firstOption;
		this.secondOption = secondOption;
	}

	public Word(Word other)
	{
		this.firstOption = other.firstOption;
		this.secondOption = other.secondOption;
	}

	public Word()
	{
		this.firstOption = "";
		this.secondOption = "";
	}

	public boolean equals(Word w)
	{
		return ( this.firstOption.equals(w.getFirstOption())
			&& this.secondOption.equals(w.getSecondOption()));
	}

	public String getFirstOption()
	{
		return firstOption;
	}

	public void setFirstOption(String firstOption)
	{
		this.firstOption = firstOption;
	}

	public String getSecondOption()
	{
		return secondOption;
	}

	public void setSecondOption(String secondOption)
	{
		this.secondOption = secondOption;
	}
}
