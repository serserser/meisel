package dom.meisel.mainApp;

import dom.meisel.Utils.BaseClass;
import dom.meisel.Utils.Dialogs.InformationDialog;
import dom.meisel.dataobjects.Word;
import dom.meisel.editor.editor.EditorController;
import dom.meisel.mainApp.Learning.LearningManager;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;
import org.apache.log4j.Logger;

import java.io.IOException;

public class MainController extends BaseClass
{
	private static MainController instance;

	final static Logger logger = Logger.getLogger(MainController.class);
	private LearningManager learningManager;

	@FXML
	private Label labelLang1;

	@FXML
	private Label labelLang2;

	@FXML
	private TextField textFieldLang1;

	@FXML
	private TextField textFieldLang2;

	@FXML
	private Label labelTip;

	@FXML
	private Label labelQuizName;

	@FXML
	private ProgressBar progressBarCurrentRound;

	@FXML
	private ProgressBar progressBarOverall;

	@FXML
	Menu menuQuiz;

	public MainController()
	{
		learningManager = new LearningManager();
		loadPreviouslyUsedTests();
	}

	public static MainController getInstance()
	{
		if ( instance == null )
		{
			instance = new MainController();
		}
		return instance;
	}

	@FXML
	@SuppressWarnings("unused")
	private void createNewTestHandler(@SuppressWarnings("unused") ActionEvent event) throws IOException
	{
		String methodName = "createNewTestHandler";
		logStart(methodName);

		EditorController editor = EditorController.getInstance();
		editor.openNewQuiz(null);

		logger.info("Naciśnięto 'stworz nowy'");
		logEnd(methodName);
	}

	@FXML
	@SuppressWarnings("unused")
	private void onButtonStartLearningClicked(@SuppressWarnings("unused") ActionEvent event)
	{
		learningManager.startLearning();
		sendNextWordToForm();
		setCurrentRoundProgress(0);
		setOverallProgress(0);
	}

	@FXML
	@SuppressWarnings("unused")
	private void onAcceptButtonClicked(@SuppressWarnings("unused") ActionEvent event)
	{
		clearTip();
		logger.trace("onAcceptButtonClicked");
		Word answer = new Word(textFieldLang1.getText(), textFieldLang2.getText());
		boolean answerCorrect = learningManager.checkAnswer(answer);
		if ( answerCorrect )
		{
			logger.info("answer was correct");
		}
		else
		{
			logger.info("answer was wrong");
			setTip(learningManager.getCurrentWord());
		}
		sendNextWordToForm();
		setCurrentRoundProgress(learningManager.getCurrentRoundProgress());
		setOverallProgress(learningManager.getOverallProgress());
	}

	@FXML
	@SuppressWarnings("unused")
	private void loadTestHandler(@SuppressWarnings("unused") ActionEvent event)
	{
		learningManager.loadQuiz();
		setCurrentRoundProgress(0);
		setOverallProgress(0);
		refreshQuiz();
		textFieldLang1.setDisable(true);
	}

	@FXML
	@SuppressWarnings("unused")
	private void loadLatestHandler(@SuppressWarnings("unused") ActionEvent event)
	{
		logger.info("Not implemented so far");
		/*
		 * wyciągnąć informację z preferences
		 * wczytać dane z testu o danej ścieżce
		 * wpisać ścieżkę na szczyt listy ścieżek
		 */
	}

	@FXML
	@SuppressWarnings("unused")
	private void closeTestHandler(@SuppressWarnings("unused") ActionEvent event)
	{
		learningManager.closeQuiz();
	}

	@FXML
	@SuppressWarnings("unused")
	private void openQuizHandler(@SuppressWarnings("unused") ActionEvent event)
	{
		String methodName = "loadTestHandler";
		logStart(methodName);

		EditorController editor = EditorController.getInstance();
		editor.openExistingQuiz(null);

		logEnd(methodName);
	}

	private void sendNextWordToForm()
	{
		if ( learningManager.hasNextWord() )
		{
			Word current = learningManager.getNextWord();
			setWordContentInForm(current);
			textFieldLang2.requestFocus();
		}
		else
		{
			InformationDialog.show("Zakończono naukę");
			clearTip();
			textFieldLang1.clear();
			textFieldLang2.clear();
			textFieldLang1.setDisable(false);
			textFieldLang2.setDisable(false);
		}
	}

	private void setWordContentInForm(Word w)
	{
		textFieldLang1.setText(w.getFirstOption());
		textFieldLang2.setText("");
	}

	private void refreshQuiz()
	{
		labelLang1.setText(learningManager.getQuiz().getFirstColumnLanguage());
		labelLang2.setText(learningManager.getQuiz().getSecondColumnLanguage());
		labelQuizName.setText(learningManager.getQuiz().getName() + " - " + learningManager.getQuiz().getDescription());
	}

	private void setTip(Word word)
	{
		labelTip.setText(word.getFirstOption() + " - " + word.getSecondOption());
	}

	private void clearTip()
	{
		labelTip.setText("");
	}

	public void setCurrentRoundProgress(double progress)
	{
		progressBarCurrentRound.setProgress(progress);
	}

	public void setOverallProgress(double progress)
	{
		progressBarOverall.setProgress(progress);
	}

	private void loadPreviouslyUsedTests()
	{

	}
}
