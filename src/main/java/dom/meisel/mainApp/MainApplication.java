package dom.meisel.mainApp;

import dom.meisel.editor.editor.EditorController;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.apache.log4j.Logger;
import java.io.IOException;

public class MainApplication extends Application
{
	private static Logger logger = Logger.getLogger(MainApplication.class);


	@Override
	public void start(Stage primaryStage)
	{
		try
		{
			Parent root = FXMLLoader.load(getClass().getResource("/fxml/mainForm.fxml"));
			primaryStage.setTitle("Program meisel");
			primaryStage.setScene(new Scene(root));
			Image icon = new Image(getClass().getResourceAsStream("/images/ikona.png"));
			primaryStage.getIcons().add(icon);

			primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>()
			{
				@Override
				public void handle(WindowEvent windowEvent)
				{
					logger.debug("onCloseRequest action performed");
					boolean canBeClosed = EditorController.getInstance().checkAndAssureThatNoChangesWillBeLost();
					if ( ! canBeClosed )
					{
						windowEvent.consume();
						return;
					}

					Platform.exit();
				}
			});
			primaryStage.show();
		} catch ( IOException exc )
		{
			exc.printStackTrace();
			String message = "Failed to load FXML for primaryStage due to " + exc.getMessage();
			logger.error(message);
		}
	}

	public static void main(String[] args)
	{
		launch(args);
	}
}
