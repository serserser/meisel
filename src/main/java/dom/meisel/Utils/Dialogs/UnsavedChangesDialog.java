package dom.meisel.Utils.Dialogs;

import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;

public class UnsavedChangesDialog extends Alert
{
	public UnsavedChangesDialog()
	{
		super(AlertType.WARNING);
		setButtons();
		setDefaultLabels();
	}

	public UnsavedChangesDialog(String contextText, String headerText, String title)
	{
		super(AlertType.WARNING);
		setContentText(contextText);
		setHeaderText(headerText);
		setTitle(title);

		setButtons();
	}

	private void setButtons()
	{
		getButtonTypes().clear();
		getButtonTypes().addAll(ButtonType.YES, ButtonType.NO, ButtonType.CANCEL);

		Button yesButton = (Button) getDialogPane().lookupButton(ButtonType.YES);
		Button noButton = (Button) getDialogPane().lookupButton(ButtonType.NO);
		Button cancelButton = (Button) getDialogPane().lookupButton(ButtonType.CANCEL);

		yesButton.setDefaultButton(true);
		noButton.setDefaultButton(false);
		cancelButton.setDefaultButton(false);
	}

	private void setDefaultLabels()
	{
		setTitle("Niezapisane zmiany");
		setHeaderText("Quiz otwarty w edytorze ma niezapisane zmiany.");
		setContentText("Czy chcesz zapisać zmiany?");
	}
}
