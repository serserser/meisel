package dom.meisel.Utils.FileOperations;

import dom.meisel.Utils.BaseClass;
import dom.meisel.dataobjects.Quiz;

import javax.xml.bind.*;
import java.io.File;
import java.io.IOException;

public class QuizFileHandler extends BaseClass
{
	public static void save(Quiz quiz, String filePath) throws IOException
	{
		String methodName = "save";
		try
		{
			File xmlFile = new File(filePath);
			JAXBContext jaxbContext = JAXBContext.newInstance(Quiz.class);
			logger.trace("Log after getting JAXBContext");

			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			logger.trace("Log after getting JAXB Marshaller");
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

			jaxbMarshaller.marshal(quiz, xmlFile);
			logger.trace("Log after marshaling to file");
		} catch ( JAXBException exc )
		{
			String message = "Failed to marshall quiz file: " + filePath;
			logger.warn(methodName + " " + message);
			throw new IOException(message);
		}
	}

	public static Quiz load(String filePath) throws IOException
	{
		String methodName = "load";
		logStart(methodName);

		Quiz quiz;
		try
		{
			File xmlFile = new File(filePath);
			logger.trace("after creating file");
			JAXBContext jaxbContext = JAXBContext.newInstance(Quiz.class);
			logger.trace("after getting instace");

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			quiz = (Quiz) jaxbUnmarshaller.unmarshal(xmlFile);
			logger.info("Unmarshalled and loaded file " + filePath);

		} catch ( JAXBException exc )
		{
			String message = "Failed to unmarshall quiz file: " + filePath;
			logger.warn(methodName + " " + message);
			throw new IOException(message);
		}

		logEnd(methodName);
		return quiz;
	}
}
