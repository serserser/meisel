package dom.meisel.Learning;


import dom.Utils.BaseClass;
import dom.Utils.Exceptions.NoMoreWordsException;
import dom.Utils.FileOperations;
import dom.Utils.InformationDialog;
import dom.Utils.QuizFileHandler;
import dom.dataobjects.Quiz;
import dom.dataobjects.Word;

import java.io.IOException;

public class LearningManager extends BaseClass
{
	public enum OpeningStatus
	{
		OPENED,
		NOT_OPENED
	}

	public enum LearningStatus
	{
		NOT_RUNNING,
		RUNNING
	}

	private Quiz openedQuiz;
	private QuizWrapper quizWrapper;
	private OpeningStatus openingStatus;
	private LearningStatus learningStatus;
	private Word currentWord;

	public LearningManager()
	{
		initialize();
	}

	private void initialize()
	{
		openedQuiz = new Quiz();
		quizWrapper = new QuizWrapper();
		learningStatus = LearningStatus.NOT_RUNNING;
		openingStatus = OpeningStatus.NOT_OPENED;
	}

	public void loadQuiz()
	{
		String openFilePath = FileOperations.getOpenFilePath(null);
		if ( openFilePath == null )
			return;

		try
		{
			Quiz quiz = QuizFileHandler.load(openFilePath);
			quizWrapper.setQuiz(quiz);
			openedQuiz = quiz;
			openingStatus = OpeningStatus.OPENED;
		} catch ( IOException exc )
		{
			String message = "Failed to open quiz due to " + exc.getMessage();
			logger.warn(message);
			InformationDialog.show(message, "Error");
		}
	}

	public void startLearning()
	{
		quizWrapper.createNewRound();
		quizWrapper.resetProgressCounters();
	}

	public boolean hasNextWord()
	{
		if ( ! quizWrapper.hasNextWord() )
		{
			quizWrapper.resetProgressCounters();
		}
		return quizWrapper.hasNextWord();
	}

	public Word getNextWord()
	{
		if ( quizWrapper.hasNextWord() )
		{
			currentWord = quizWrapper.getNextWord();
			return currentWord;
		}
		else
			throw new NoMoreWordsException("There are no words in this quiz.");
	}

	public boolean checkAnswer(Word w)
	{
		if ( w.equals(currentWord) )
		{
			return true;
		}
		else
		{
			quizWrapper.addWordToNextRound(currentWord);
			return false;
		}
	}

	public Word getCurrentWord()
	{

		return currentWord;
	}

	public void closeQuiz()
	{
		resetQuiz();
	}

	public void setQuizStatus(LearningStatus status)
	{
		learningStatus = status;
	}

	public Quiz getQuiz()
	{
		return openedQuiz;
	}

	private void resetQuiz()
	{
		openedQuiz = new Quiz();
		quizWrapper.reset();
		learningStatus = LearningStatus.NOT_RUNNING;
		openingStatus = OpeningStatus.NOT_OPENED;
	}

	public double getOverallProgress()
	{
		return quizWrapper.getOverallProgress();
	}

	public double getCurrentRoundProgress()
	{
		return quizWrapper.getCurrentRoundProgress();
	}
}
