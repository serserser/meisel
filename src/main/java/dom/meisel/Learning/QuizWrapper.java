package dom.meisel.Learning;

import dom.Utils.BaseClass;
import dom.Utils.Exceptions.NoMoreWordsException;
import dom.dataobjects.Quiz;
import dom.dataobjects.Word;
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;


/**
 * Provides words for the learning process
 */
public class QuizWrapper extends BaseClass
{
	private static int REPEATS = 2;
	private static int RETAKES = 2;


	List<Word> wordsFromQuiz;
	LinkedList<Word> currentRound;
	ArrayList<Word> nextRound;

	int numberOfRepeats;
	int numberOfRetakes;

	int numberOfUnansweredQuestions;
	int numberOfCorrectlyAnsweredQuestions;

	int initialRoundSize;

	public QuizWrapper()
	{
		wordsFromQuiz = new ArrayList<>();
		currentRound = new LinkedList<>();
		nextRound = new ArrayList<>();
		numberOfRepeats = REPEATS;
		numberOfRetakes = RETAKES;
		// TODO : now it's hardcoded. Change it, so that it is available for configuration
	}

	public void reset()
	{
		wordsFromQuiz.clear();
		currentRound.clear();
		nextRound.clear();
	}

	public void setQuiz(Quiz quiz)
	{
		wordsFromQuiz = quiz.getWords();
	}

	public boolean hasNextWord()
	{
		if ( (currentRound.size() == 0) && (nextRound.size() > 0) )
		{
			currentRound = createRound(nextRound);
			nextRound.clear();
		}
		return (currentRound.size() > 0);
	}

	public void createNewRound()
	{
		currentRound = createRound(wordsFromQuiz);
	}

	public Word getNextWord() throws NoMoreWordsException
	{
		if ( !hasNextWord() )
			throw new NoMoreWordsException("There are no words in this quiz.");
		numberOfCorrectlyAnsweredQuestions++;
		return currentRound.removeFirst();
	}

	public void addWordToNextRound(Word word)
	{
		nextRound.add(word);
		numberOfCorrectlyAnsweredQuestions--;
		numberOfUnansweredQuestions += numberOfRetakes;
	}

	private LinkedList<Word> createRound(List<Word> sourceWords)
	{
		currentRound.clear();
		logger.info("Creating new round");
		LinkedList<Word> result = new LinkedList<>();
		int numberOfWords = sourceWords.size();

		List<WordWrapper> words = new ArrayList<>();

		for ( Word w : sourceWords )
		{
			words.add(new WordWrapper(numberOfRepeats, new Word(w)));
		}

		Random randomGenerator = new Random();

		boolean moreWordsToBoAdded;
		do
		{
			int wordIndex = randomGenerator.nextInt(numberOfWords);
			WordWrapper wordStruct = words.get(wordIndex);
			if ( wordStruct.amount != 0 )
			{
				words.get(wordIndex).amount--;
				result.addLast(wordStruct.word);
				moreWordsToBoAdded = true;
			}
			else
			{
				moreWordsToBoAdded = false;
				int distanceFromCurrentWord = 1;
				while ( wordIndex - distanceFromCurrentWord >= 0
						|| wordIndex + distanceFromCurrentWord < numberOfWords )
				{
					int upperNeighbourIndex = wordIndex + distanceFromCurrentWord;
					int lowerNeighbourIndex = wordIndex - distanceFromCurrentWord;
					if ( upperNeighbourIndex < numberOfWords )
					{
						if ( words.get(upperNeighbourIndex).amount > 0 )
						{
							words.get(upperNeighbourIndex).amount--;
							result.addLast(words.get(upperNeighbourIndex).word);
							moreWordsToBoAdded = true;
							break;
						}
					}
					if ( lowerNeighbourIndex >= 0 )
					{
						if ( words.get(lowerNeighbourIndex).amount > 0 )
						{
							words.get(lowerNeighbourIndex).amount--;
							result.addLast(words.get(lowerNeighbourIndex).word);
							moreWordsToBoAdded = true;
							break;
						}
					}
					distanceFromCurrentWord++;
				}
			}
		} while ( moreWordsToBoAdded );
		initialRoundSize = result.size();
		return result;
	}

	public double getOverallProgress()
	{
		double result = numberOfCorrectlyAnsweredQuestions / (double) numberOfUnansweredQuestions;
		return result;
	}

	public double getCurrentRoundProgress()
	{
		double numerator = initialRoundSize - currentRound.size();
		double denominator = initialRoundSize;
		return ( numerator  / denominator );
	}

	public void resetProgressCounters()
	{
		numberOfUnansweredQuestions = initialRoundSize;
		numberOfCorrectlyAnsweredQuestions = 0;
	}

	class WordWrapper
	{
		public Word word;
		public int amount;

		public WordWrapper(int amount, Word word)
		{
			this.amount = amount;
			this.word = word;
		}
	}
}
