package dom.meisel.editor.editor;

import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.apache.log4j.Logger;

import java.io.IOException;

public class EditorStage extends Stage
{
	Logger logger = Logger.getLogger(getClass());
	private static EditorStage instance;
	public static final String windowTitle = "Editor";

	public static EditorStage getInstance()
	{
		if ( instance == null )
		{
			instance = new EditorStage();
		}
		return instance;
	}

	public EditorStage()
	{
		try
		{
			Parent editorLayout = FXMLLoader.load(getClass().getResource("/fxml/editorForm.fxml"));
			setScene(new Scene(editorLayout, 600, 300));
			setTitle(windowTitle);
			instance = this;
			EditorController.getInstance().initialize();

			setOnCloseRequest(new EventHandler<WindowEvent>()
			{
				@Override
				public void handle(WindowEvent windowEvent)
				{

					boolean result = EditorController.getInstance().checkAndAssureThatNoChangesWillBeLost();
					if ( result )
					{
						EditorController.getInstance().performCleanupBeforeExit();
						hide();
					} else
					{
						windowEvent.consume();
					}
				}
			});
		} catch ( IOException exc )
		{
			System.err.println("Złapano IOException");
			exc.printStackTrace();
		}

	}

	public void resetWindowTitle()
	{
		setTitle(windowTitle);
	}
}