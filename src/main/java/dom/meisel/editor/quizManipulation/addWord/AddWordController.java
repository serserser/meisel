package dom.meisel.editor.quizManipulation.addWord;

import dom.meisel.Utils.BaseClass;
import dom.meisel.dataobjects.Word;
import dom.meisel.editor.editor.EditorController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class AddWordController extends BaseClass
{

	@FXML
	private Label labelLang1;

	@FXML
	private Label labelLang2;

	@FXML
	private TextField textFieldLang1;

	@FXML
	private TextField textFieldLang2;

	private static AddWordController instance;

	private boolean editingExistingWord;
	Word editedWord;

	public AddWordController()
	{
		instance = this;
		editingExistingWord = false;
	}

	public static AddWordController getInstance()
	{
		if ( instance == null )
		{
			instance = new AddWordController();
		}
		AddWordStage.getInstance();
		return instance;
	}

	@FXML
	private void onClearButtonClicked(ActionEvent event)
	{
		clearFields();
	}

	@FXML
	private void onOKButtonClicked(ActionEvent event)
	{
		String methodName = "onOKButtonClicked";
		logStart(methodName);

		String firstOption = textFieldLang1.getText();
		String secondOption = textFieldLang2.getText();

		if ( "".equals(firstOption) || "".equals(secondOption) )
			return;

		Word word = new Word(firstOption, secondOption);

		EditorController editor = EditorController.getInstance();

		if ( editingExistingWord )
		{
			EditorController.getInstance().modifyWordInTable(editedWord, word);
			performCleanup();
		}
		else
		{
			editor.addWordToQuiz(word);
		}

		logEnd(methodName);
		clearFields();
	}

	@FXML
	private void handleExitCombination(KeyEvent event)
	{
		if ( event.isControlDown() && event.getCode() == KeyCode.Q )
		{
			performCleanupAndExit();
		}
	}

	public void performCleanup()
	{
		clearFields();
		textFieldLang1.requestFocus();
		editingExistingWord = false;
	}

	public void performCleanupAndExit()
	{
		performCleanup();
		AddWordStage.getInstance().hide();
	}

	public void clearFields()
	{
		textFieldLang1.clear();
		textFieldLang2.clear();
		textFieldLang1.requestFocus();
	}

	public void setColumnDescriptions(String firstColumnDescription, String secondColumnDescription)
	{
		labelLang1.setText(firstColumnDescription);
		labelLang2.setText(secondColumnDescription);
	}

	public void setWordForEditing(Word word)
	{
		textFieldLang1.setText(word.getFirstOption());
		textFieldLang2.setText(word.getSecondOption());
		editedWord = word;
		editingExistingWord = true;
	}
}
