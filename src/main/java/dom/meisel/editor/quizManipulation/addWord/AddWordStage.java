package dom.meisel.editor.quizManipulation.addWord;

import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.apache.log4j.Logger;

import java.io.IOException;

public class AddWordStage extends Stage
{
	Logger logger = Logger.getLogger(getClass());

	public static AddWordStage instance;

	public static AddWordStage getInstance()
	{
		if ( instance == null )
		{
			instance = new AddWordStage();
		}
		return instance;
	}

	public AddWordStage()
	{
		try
		{
			Parent addWordLayout = FXMLLoader.load(getClass().getResource("/fxml/addWordForm.fxml"));
			setScene(new Scene(addWordLayout, 600, 150));
			setTitle("Dodaj słowo");
			instance = this;

			setOnCloseRequest(new EventHandler<WindowEvent>()
			{
				@Override
				public void handle(WindowEvent windowEvent)
				{
					AddWordController.getInstance().performCleanupAndExit();
				}
			});
		} catch ( IOException exc )
		{
			String message = "Failed to load FXML for addWordStage due to " + exc.getMessage();
			logger.error(message);
		}
	}
}
