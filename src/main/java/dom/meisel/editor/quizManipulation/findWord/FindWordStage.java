package dom.meisel.editor.quizManipulation.findWord;

import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.apache.log4j.Logger;

import java.io.IOException;

public class FindWordStage extends Stage
{
	Logger logger = Logger.getLogger(getClass());

	public static FindWordStage instance;

	public static FindWordStage getInstance()
	{
		if ( instance == null )
		{
			instance = new FindWordStage();
		}
		return instance;
	}

	public FindWordStage()
	{
		try
		{
			Parent addWordLayout = FXMLLoader.load(getClass().getResource("/fxml/findWord.fxml"));
			setScene(new Scene(addWordLayout, 600, 150));
			setTitle("Znajdź");
			instance = this;

			setOnCloseRequest(new EventHandler<WindowEvent>()
			{
				@Override
				public void handle(WindowEvent windowEvent)
				{
					// TODO : cleanup the widget before exiting
					// save state of the settings
					logger.info("widget closing requested");
				}
			});
		} catch ( IOException exc )
		{
			String message = "Failed to load FXML for FindWordStage due to " + exc.getMessage();
			logger.error(message);
		}
	}
}
