package dom.editor.quizManipulation.findWord;

import dom.editor.editor.EditorController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;

public class FindWordController
{
	@FXML
	private TextField searchedWord;

	private static FindWordController instance;

	public static FindWordController getInstance()
	{
//		FindWordStage.getInstance();		// just to make sure, that it's already created
		if ( instance == null )
		{
			instance = new FindWordController();
		}
		return instance;
	}

	public FindWordController()
	{
		instance = this;
	}

	@FXML
	private void onOKButtonClicked(ActionEvent event)
	{
		String word = searchedWord.getText();
		EditorController.getInstance().selectWordsInTable(word);
	}

	@FXML
	private void onClearButtonClicked(ActionEvent event)
	{
		// TODO : change System.out to log4j logger
		System.out.println("Form clearing requested");
		clearFields();
	}

	public void open()
	{
		FindWordStage.getInstance().show();
	}
	private void clearFields()
	{
		searchedWord.clear();
		searchedWord.requestFocus();
	}
}
