package dom.editor.quizManipulation;

import dom.Utils.*;
import dom.Utils.Exceptions.QuizFileLoadingException;
import dom.dataobjects.Quiz;
import dom.dataobjects.Word;
import dom.editor.editor.EditorController;
import dom.editor.editor.EditorStage;
import javafx.scene.control.Alert;
import dom.dataobjects.Quiz.ClosingStatus;
import dom.dataobjects.Quiz.SavingStatus;
import dom.dataobjects.Quiz.Status;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;


import java.io.IOException;
import java.util.List;
import java.util.Optional;

public class QuizWithEditingFunctions extends BaseClass
{
	enum DuplicatedWord
	{
		FIRST_COLUMN,
		SECOND_COLUMN,
		BOTH_COLUMNS,
		NO_DUPLICATES
	}

	enum UserDecision
	{
		ADD,
		DONT_ADD
	}


	private Quiz openedQuiz;

	private String filePath;
	boolean filePathIsValid;

	public QuizWithEditingFunctions()
	{
		openedQuiz = new Quiz();
		filePathIsValid = false;
	}

	public Quiz getQuiz()
	{
		return openedQuiz;
	}

	public boolean openNewQuiz()
	{
		boolean noChangesLost = checkAndAssureThatNoChangesWillBeLost();
		if ( !noChangesLost )
			return false;

		reset();
		EditorController.getInstance().setSaveMenuItemState(true);
		return true;
	}

	public boolean openExistingQuiz() throws QuizFileLoadingException
	{
		boolean noChangesLost = checkAndAssureThatNoChangesWillBeLost();
		if ( !noChangesLost )
			return false;

		String openFilePath = FileOperations.getOpenFilePath(null);
		if ( openFilePath == null )
			return false;
		try
		{
			openedQuiz = QuizFileHandler.load(openFilePath);
			openedQuiz.setStatus(Quiz.Status.SAVED);
			EditorController.getInstance().setSaveMenuItemState(false);
			filePathIsValid = true;
			filePath = openFilePath;
			return true;
		} catch ( IOException exc )
		{
			String message = "Failed to open quiz due to: " + exc.getMessage();
			logger.warn(message);
			throw new QuizFileLoadingException(message);
		}
	}

	public SavingStatus saveQuiz()
	{
		String saveFilePath;
		if ( filePathIsValid )
		{
			saveFilePath = filePath;
		}
		else
		{
			saveFilePath = FileOperations.getSaveFilePath(null);
			if ( saveFilePath == null )
				return SavingStatus.USER_CANCELED;
			else
			{
				filePathIsValid = true;
				filePath = saveFilePath;
			}

		}

		try
		{
			QuizFileHandler.save(openedQuiz, saveFilePath);
			setQuizStatus(Quiz.Status.SAVED);
			return Quiz.SavingStatus.SUCCESS;
		} catch ( IOException exc )
		{
			String message = "Failed to save quiz due to: " + exc.getMessage();
			logger.warn(message);
			InformationDialog.show(message, "Error");
		}
		return SavingStatus.ERROR_OCCURRED;
	}

	public void removeWords(List<Word> words)
	{
		openedQuiz.removeWords(words);
		setQuizStatus(Status.UNSAVED);
	}

	public void addWord(Word word)
	{
		DuplicatedWord duplicate = checkIfWordAlreadyExists(word);

		if ( ! (duplicate == DuplicatedWord.NO_DUPLICATES ) )
		{
			UserDecision decision = promptUserAboutDuplicate(duplicate, word);
			if ( decision == UserDecision.DONT_ADD )
				return;
		}

		openedQuiz.addWord(word);
		setQuizStatus(Status.UNSAVED);
	}

	public void modifyWord(Word original, Word modified)
	{
		List<Word> items = openedQuiz.getWords();
		int index = items.indexOf(original);
		items.remove(original);
		items.add(index, modified);
		setQuizStatus(Status.UNSAVED);
	}

	private DuplicatedWord checkIfWordAlreadyExists(Word word)
	{
		Word wordAlreadyAdded = null;
		for ( Word w : openedQuiz.getWords() )
		{
			if ( w.getFirstOption().equals(word.getFirstOption()) && !w.getSecondOption().equals(word.getSecondOption()) )
			{
				return DuplicatedWord.FIRST_COLUMN;
			}
			if ( !w.getFirstOption().equals(word.getFirstOption()) && w.getSecondOption().equals(word.getSecondOption()) )
			{
				return DuplicatedWord.SECOND_COLUMN;
			}
			if ( w.getFirstOption().equals(word.getFirstOption()) && w.getSecondOption().equals(word.getSecondOption()) )
			{
				return DuplicatedWord.BOTH_COLUMNS;
			}
		}

		return DuplicatedWord.NO_DUPLICATES;
	}

	private UserDecision promptUserAboutDuplicate(DuplicatedWord duplicate, Word word)
	{
		Alert alert = new Alert(Alert.AlertType.NONE);
		alert.setTitle("Błąd");
		if ( duplicate == DuplicatedWord.BOTH_COLUMNS )
		{
			alert.setHeaderText("Takie słowo już istnieje w quizie");
			EditorController.getInstance().selectWordsInTable(word);
		}
		if ( duplicate == DuplicatedWord.FIRST_COLUMN )
		{
			alert.setHeaderText("Takie słowo już istnieje w pierwszej kolumnie quizu: " + word.getFirstOption() + " - " + word.getSecondOption());
			EditorController.getInstance().selectWordsInTable(word.getFirstOption());
		}

		if ( duplicate == DuplicatedWord.SECOND_COLUMN )
		{
			alert.setHeaderText("Takie słowo już istnieje w drugiej kolumnie quizu: " + word.getFirstOption() + " - " + word.getSecondOption());
			EditorController.getInstance().selectWordsInTable(word.getSecondOption());
		}
		alert.setContentText("Czy chcesz je dodać mimo to?");
		alert.getButtonTypes().addAll(ButtonType.YES, ButtonType.NO);

		Button yesButton = (Button) alert.getDialogPane().lookupButton(ButtonType.YES);
		Button noButton = (Button) alert.getDialogPane().lookupButton(ButtonType.NO);
		yesButton.setDefaultButton(false);
		noButton.setDefaultButton(true);

		Optional<ButtonType> result = alert.showAndWait();

		if ( result.get().equals(ButtonType.YES) )
		{
			return UserDecision.ADD;
		}
		else
		{
			return UserDecision.DONT_ADD;
		}
	}


	public boolean checkAndAssureThatNoChangesWillBeLost()
	{
		ClosingStatus closingStatus = canCloseCurrentlyOpenedQuiz();
		if ( closingStatus == Quiz.ClosingStatus.CANCEL_OPERATION )
		{
			return false;
		}
		else if ( closingStatus == ClosingStatus.SAVE_REQUESTED )
		{
			SavingStatus savingStatus = saveQuiz();
			if ( savingStatus == SavingStatus.USER_CANCELED )
			{
				return false;
			}
			else if ( savingStatus == SavingStatus.ERROR_OCCURRED )
			{
				Alert alert = new Alert(Alert.AlertType.ERROR);
				alert.setTitle("Error");
				alert.setContentText("An error occurred while saving the quiz.");
				alert.show();
				return false;
			}
		}

		return true;
	}

	public ClosingStatus canCloseCurrentlyOpenedQuiz()
	{
		if ( openedQuiz.getStatus() == Quiz.Status.UNSAVED )
		{
			UnsavedChangesAlert unsavedChanges = new UnsavedChangesAlert();
			unsavedChanges.setTitle("Niezapisane zmiany");
			unsavedChanges.setHeaderText("Quiz otwarty w edytorze ma niezapisane zmiany.");
			unsavedChanges.setContentText("Czy chcesz zapisać zmiany?");

			Optional<ButtonType> result = unsavedChanges.showAndWait();
			if ( result.get().equals(ButtonType.YES) )
			{
				logger.info("User requested to save changes");
				return ClosingStatus.SAVE_REQUESTED;
			}
			else if ( result.get().equals(ButtonType.NO) )
			{
				logger.info("User requested to discard changes");
				logger.warn("User requested to discard changes");
				return ClosingStatus.CAN_BE_CLOSED;
			}
			logger.info("User requested to cancel operation");
			return ClosingStatus.CANCEL_OPERATION;
		}
		return ClosingStatus.CAN_BE_CLOSED;
	}

	public void setQuizStatus(Status currentStatus)
	{
		Quiz.Status previousStatus = openedQuiz.getStatus();
		openedQuiz.setStatus(currentStatus);
		String title = EditorStage.getInstance().getTitle();
		if ( previousStatus == Quiz.Status.SAVED &&
				currentStatus == Quiz.Status.UNSAVED )
		{
			EditorStage.getInstance().setTitle("Edytor (UNSAVED CHANGES)");
			EditorController.getInstance().setSaveMenuItemState(true);
		}
		else if ( previousStatus == Quiz.Status.UNSAVED &&
				currentStatus == Quiz.Status.SAVED )
		{
			EditorStage.getInstance().setTitle("Edytor");
			EditorController.getInstance().setSaveMenuItemState(false);
		}
	}

	public void reset()
	{
		openedQuiz = new Quiz();
		filePathIsValid = false;
	}
}