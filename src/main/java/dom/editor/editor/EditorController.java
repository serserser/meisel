package dom.editor.editor;


import dom.Utils.BaseClass;
import dom.dataobjects.Quiz;
import dom.dataobjects.Word;
import dom.editor.quizManipulation.QuizWithEditingFunctions;
import dom.editor.quizManipulation.addWord.AddWordController;
import dom.editor.quizManipulation.addWord.AddWordStage;
import dom.editor.quizManipulation.findWord.FindWordController;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;

import java.util.List;
import java.util.function.Predicate;

public class EditorController extends BaseClass
{
	private QuizWithEditingFunctions openedQuiz;
	private static EditorController instance = null;


	@FXML
	private TextField quizNameTextField;

	@FXML
	private TextField quizDescriptionTextField;

	@FXML
	private TextField firstColumnDescriptionTextField;

	@FXML
	private TextField secondColumnDescriptionTextField;

	@FXML
	private TableView<Word> wordsTableView;

	@FXML
	private TableColumn<Word, String> firstColumn;

	@FXML
	private TableColumn<Word, String> secondColumn;


	@FXML
	private MenuItem saveMenuItem;

	public EditorController()
	{
		instance = this;
		openedQuiz = new QuizWithEditingFunctions();
	}

	public static EditorController getInstance()
	{
		EditorStage.getInstance();
		if ( instance == null )
		{
			instance = new EditorController();
		}
		return instance;
	}

	public void initialize()
	{
		wordsTableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		firstColumn.setCellValueFactory(new PropertyValueFactory<>("firstOption"));
		secondColumn.setCellValueFactory(new PropertyValueFactory<>("secondOption"));
	}

	@FXML
	@SuppressWarnings("unused")
	public void openNewQuiz(@SuppressWarnings("unused") ActionEvent event)
	{
		if ( openedQuiz.openNewQuiz() )
		{
			refreshOpenedQuiz();
			EditorStage.getInstance().resetWindowTitle();
			EditorStage.getInstance().show();
		}
		if ( event != null )
			event.consume();
	}

	public void openExistingQuiz(@SuppressWarnings("unused") ActionEvent event)
	{
		if ( openedQuiz.openExistingQuiz() )
		{
			refreshOpenedQuiz();
			EditorStage.getInstance().resetWindowTitle();
			EditorStage.getInstance().show();
		}
	}

	@FXML
	public void saveOpenedQuiz()
	{
		openedQuiz.saveQuiz();
	}


	@FXML
	@SuppressWarnings("unused")
	private void onAddClicked(@SuppressWarnings("unused") MouseEvent event)
	{
		showAddWordStage();
	}

	@FXML
	@SuppressWarnings("unused")
	private void onAddMenuItemChosen(@SuppressWarnings("unused") ActionEvent event)
	{
		showAddWordStage();
	}

	@FXML
	@SuppressWarnings("unused")
	private void onDeleteMenuItemChosen(@SuppressWarnings("unused") ActionEvent event)
	{
		onRemoveClicked(null);
	}

	@FXML
	@SuppressWarnings("unused")
	private void onRemoveClicked(@SuppressWarnings("unused") MouseEvent event)
	{
		String methodName = "onRemoveClicked";
		logStart(methodName);

		ObservableList<Word> words = wordsTableView.getSelectionModel().getSelectedItems();
		openedQuiz.removeWords(words);
		wordsTableView.getSelectionModel().clearSelection();

		logEnd(methodName);
	}

	@FXML
	@SuppressWarnings("unused")
	private void onFindClicked(@SuppressWarnings("unused") MouseEvent event)
	{
		String methodName = "onFindClicked";
		logStart(methodName);

		FindWordController finder = FindWordController.getInstance();
		finder.open();

		logEnd(methodName);
	}

	@FXML
	@SuppressWarnings("unused")
	private void onFindMenuItemChosen(@SuppressWarnings("unused") ActionEvent event)
	{
		String methodName = "onFindClicked";
		logStart(methodName);

		FindWordController finder = FindWordController.getInstance();
		finder.open();

		logEnd(methodName);
	}

	@FXML
	@SuppressWarnings("unused")
	private void onQuizDataChanged(@SuppressWarnings("unused") KeyEvent event)
	{
		if ( event.isControlDown() )
			return;
		updateOpenedQuiz();
	}

	@FXML
	@SuppressWarnings("unused")
	private void onColumnsDescriptionChanged(@SuppressWarnings("unused") KeyEvent event)
	{
		if ( event.isControlDown() || (event.getCode().equals(KeyCode.CONTROL)) )
			return;
		updateOpenedQuiz();
		firstColumn.setText(firstColumnDescriptionTextField.getText());
		secondColumn.setText(secondColumnDescriptionTextField.getText());
	}

	@FXML
	@SuppressWarnings("unused")
	private void onWordsTableClicked(@SuppressWarnings("unused") MouseEvent event)
	{
		if ( event.isPrimaryButtonDown() && event.getClickCount() == 2 )
		{
			logger.info("Word in wordTable double-clicked");
			Word edited = wordsTableView.getSelectionModel().getSelectedItem();
			System.out.print(edited.getFirstOption());
			System.out.print(edited.getSecondOption());
			AddWordController.getInstance().setWordForEditing(edited);
			showAddWordStage();
		}
	}

	public void modifyWordInTable(Word original, Word modified)
	{
		openedQuiz.modifyWord(original, modified);
		wordsTableView.getSelectionModel().clearSelection();
	}

	private void refreshOpenedQuiz()
	{
		EditorStage.getInstance();
		quizNameTextField.setText(openedQuiz.getQuiz().getName());
		quizDescriptionTextField.setText(openedQuiz.getQuiz().getDescription());
		firstColumnDescriptionTextField.setText(openedQuiz.getQuiz().getFirstColumnLanguage());
		secondColumnDescriptionTextField.setText(openedQuiz.getQuiz().getSecondColumnLanguage());
		firstColumn.setText(firstColumnDescriptionTextField.getText());
		secondColumn.setText(secondColumnDescriptionTextField.getText());
		wordsTableView.setItems(openedQuiz.getQuiz().getWords());
	}

	void showAddWordStage()
	{
		AddWordStage addWordStage = AddWordStage.getInstance();
		String firstColumnDescription = openedQuiz.getQuiz().getFirstColumnLanguage();
		String secondColumnDescription = openedQuiz.getQuiz().getSecondColumnLanguage();
		AddWordController.getInstance().setColumnDescriptions(firstColumnDescription, secondColumnDescription);
		addWordStage.show();
	}

	private void updateOpenedQuiz()
	{
		openedQuiz.getQuiz().setName(quizNameTextField.getText());
		openedQuiz.getQuiz().setDescription(quizDescriptionTextField.getText());
		openedQuiz.getQuiz().setFirstColumnLanguage(firstColumnDescriptionTextField.getText());
		openedQuiz.getQuiz().setSecondColumnLanguage(secondColumnDescriptionTextField.getText());
		openedQuiz.setQuizStatus(Quiz.Status.UNSAVED);
	}

	public void addWordToQuiz(Word word)
	{
		openedQuiz.addWord(word);
	}

	public void selectWordsInTable(String searchedString)
	{
		FilteredList<Word> found = new FilteredList<Word>(openedQuiz.getQuiz().getWords(), new Predicate<Word>()
		{
			@Override
			public boolean test(Word word)
			{
				if ( (word.getFirstOption().equals(searchedString))
						|| (word.getSecondOption().equals(searchedString)) )
					return true;

				return false;
			}
		});

		wordsTableView.getSelectionModel().clearSelection();
		for ( Word w : found )
		{
			wordsTableView.getSelectionModel().select(w);
		}
	}

	public void selectWordsInTable(Word searchedWord)
	{
		FilteredList<Word> found = new FilteredList<Word>(openedQuiz.getQuiz().getWords(), new Predicate<Word>()
		{
			@Override
			public boolean test(Word word)
			{
				if ( word.getFirstOption().equals(searchedWord.getFirstOption()) &&
						word.getSecondOption().equals(searchedWord.getSecondOption()) )
					return true;

				return false;
			}
		});

		wordsTableView.getSelectionModel().clearSelection();
		for ( Word w : found )
		{
			wordsTableView.getSelectionModel().select(w);
		}
	}

	public void performCleanupBeforeExit()
	{
		openedQuiz.reset();
		refreshOpenedQuiz();
	}

	public boolean checkAndAssureThatNoChangesWillBeLost()
	{
		return openedQuiz.checkAndAssureThatNoChangesWillBeLost();
	}

	public void setSaveMenuItemState(boolean enabled)
	{
		if ( enabled )
			saveMenuItem.setDisable(false);
		else
			saveMenuItem.setDisable(true);
	}
}
