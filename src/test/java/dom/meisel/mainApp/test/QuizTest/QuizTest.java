package dom.meisel.mainApp.test.QuizTest;

import dom.meisel.dataobjects.Quiz;
import dom.meisel.dataobjects.Word;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class QuizTest
{
	private Quiz unitUnderTest;

	@Before
	public void instantiateUnitUnderTest()
	{
		unitUnderTest = new Quiz();
	}

	@Test
	public void testUnitUnderTestIsNotNull()
	{
		Assert.assertNotNull("The instantiated object should not be null", unitUnderTest);
	}

	@Test
	public void testDefaultConstructor()
	{
		Assert.assertNotNull("The name of the newly created quiz should not be null", unitUnderTest.getName());
		Assert.assertNotNull("The description of the newly created quiz should not be null", unitUnderTest.getDescription());
		Assert.assertNotNull("The description of the first column in the newly created quiz should not be null", unitUnderTest.getFirstColumnLanguage());
		Assert.assertNotNull("The description of the second column in the newly created quiz should not be null", unitUnderTest.getSecondColumnLanguage());
		Assert.assertNotNull("The words list in the newly created quiz should not be null", unitUnderTest.getWords());
		Assert.assertNotNull("The status in the newly created quiz should not be null", unitUnderTest.getStatus());
	}

	@Test
	public void assertQuizContainsProperNumberOfWords()
	{
		Word firstTestWord = new Word("first", "second");
		Word secondTestWord = new Word();
		unitUnderTest.addWord(firstTestWord);
		unitUnderTest.addWord(secondTestWord);
		List<Word> result = unitUnderTest.getWords();

		Assert.assertEquals("After adding two words, the quiz should contain two words", 2, result.size());
	}

	@Test
	public void assertQuizContainsAddedWord()
	{
		Word testWord = new Word();
		unitUnderTest.addWord(testWord);
		List<Word> result = unitUnderTest.getWords();

		boolean containsTestWord = result.contains(testWord);
		Assert.assertTrue("The quiz should contain the word, that was added to it", containsTestWord);
	}
}
