package dom.meisel.mainApp.test;

import dom.meisel.dataobjects.Word;
import org.junit.Assert;
import org.junit.Test;

public class WordTest
{
	@Test
	public void instantiateUsingDefaultConstructor()
	{
		Word unitUnderTest = new Word();

		Assert.assertNotNull("The instantiated Word object should not be null", unitUnderTest);

		String firstOption = unitUnderTest.getFirstOption();
		String secondOption = unitUnderTest.getSecondOption();

		Assert.assertEquals("The initial value for first option should be\"\"", "", firstOption);
		Assert.assertEquals("The initial value for first option should be\"\"", "", secondOption);
	}

	@Test
	public void instantiateUsingSecondConstructor()
	{
		String firstArg = "firstArg";
		String secondArg = "secondArg";

		Word unitUnderTest = new Word(firstArg, secondArg);

		Assert.assertNotNull("The instantiated Word object should not be null", unitUnderTest);

		String firstResult = unitUnderTest.getFirstOption();
		String secondResult = unitUnderTest.getSecondOption();

		Assert.assertEquals("The string retreived by getter should be the same as the one passed in the constructor", firstArg, firstResult);
		Assert.assertEquals("The string retreived by getter should be the same as the one passed in the constructor", secondArg, secondResult);
	}
}
