package dom.meisel.test.QuizTest;

import dom.dataobjects.Quiz;
import dom.dataobjects.Word;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class RemovingWordsTest
{
	private Quiz unitUnderTest;
	private Word firstTestWord;
	private Word secondTestWord;
	private Word thirdWord;


	@Before
	public void instantiateUnitUnderTestAndTestObjects()
	{
		unitUnderTest = new Quiz();
		firstTestWord = new Word("pierwsza", "druga");
		secondTestWord = new Word("pierwsza", "drugaAleInna");
		thirdWord = new Word("pierwszaAleInna", "druga");

		unitUnderTest.addWord(firstTestWord);
		unitUnderTest.addWord(secondTestWord);
		unitUnderTest.addWord(thirdWord);
	}

	@Test
	public void testRemovingSingleWord()
	{
		List<Word> argument = new ArrayList<>();
		argument.add(firstTestWord);
		unitUnderTest.removeWords(argument);

		Assert.assertFalse("The quiz shouldn't contain a word that has been deleted", unitUnderTest.getWords().contains(firstTestWord));
		Assert.assertTrue("The quiz should contain a word that hasn't been deleted", unitUnderTest.getWords().contains(secondTestWord));
		Assert.assertTrue("The quiz should contain a word that hasn't been deleted", unitUnderTest.getWords().contains(thirdWord));
	}

	@Test
	public void testRemovingMultipleWords()
	{
		List<Word> argument = new ArrayList<>();
		argument.add(firstTestWord);
		argument.add(thirdWord);

		unitUnderTest.removeWords(argument);
		Assert.assertFalse("The quiz shouldn't contain a word that has been deleted", unitUnderTest.getWords().contains(firstTestWord));
		Assert.assertTrue("The quiz should contain a word that hasn't been deleted", unitUnderTest.getWords().contains(secondTestWord));
		Assert.assertFalse("The quiz should contain a word that hasn't been deleted", unitUnderTest.getWords().contains(thirdWord));
	}
}
